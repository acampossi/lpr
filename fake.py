import os
import urllib
import cv2
import shutil
import requests
from multiprocessing import Pool
import os.path
from Processing_data import CropImage
import numpy as np
def getImageRequest(image_data):
	url,path = image_data
	if not os.path.isfile(path): 
		try:
			response = requests.get(url, stream=True,timeout=10)

			with open(path, 'wb') as out_file:
				shutil.copyfileobj(response.raw, out_file)
			del response
			print path
		except:
			pass
	else:
		print "Imagen Existente!!!"
	
def scan_plates(platedetector, input_path):
	
	img = cv2.imread(input_path)
	if img is None:
		os.remove(input_path)
		return
	# print img.shape
	
	# gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
	# plates = platedetector.detectMultiScale(gray, 1.1,5)
	# print plates
	# if plates == ():
	# 	os.remove(input_path)
	# else:
	# 	cv2.imshow('img',img)
	# 	cv2.waitKey(0)

def scan_all_images(OUTPUT_DIRECTORY):
	dirlist = os.listdir(OUTPUT_DIRECTORY)
	print len(dirlist)
	cnt = 0
	cnt2 = 0
	for image_path in dirlist:
		cnt +=1

		input_path = os.path.join(OUTPUT_DIRECTORY, image_path)
		print input_path
		if os.path.getsize(input_path)<100000:
			 os.remove(input_path)
		# else:
		# 	img = cv2.imread(input_path)
		# 	if img is None:
		# 		os.remove(input_path)
		# 	else:
		# 		print input_path,img.shape
		# 		cnt2+=1
		print cnt2,cnt
    #     gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    #     plates = platedetector.detectMultiScale(gray, 1.2,10)
    #     print plates
    #     if plates == ():
    #     	rem_imgs += 1
    #         os.remove(input_path)
    # return rem_imgs

def getfp(OUTPUT_DIRECTORY,platedetector,scale=1.1, neighbors=1):
	dirlist = os.listdir(OUTPUT_DIRECTORY)
	l = len(dirlist)
	falses_positives_list = []
	
	cnt = 0.
	for image_path in dirlist:
		cnt +=1.
		print 100*cnt/l, len(falses_positives_list)
		input_path = os.path.join(OUTPUT_DIRECTORY, image_path)
		if os.path.getsize(input_path)<2300:
			 os.remove(input_path)
		else:
			img = cv2.imread(input_path)
			if img is None:
				os.remove(input_path)
			else:
				gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
				plates = platedetector.detectMultiScale(gray, scale,neighbors)
				for plate in plates:
					crop =CropImage(img,plate)
					# cv2.imshow('false',crop)
					# cv2.waitKey(0)
					falses_positives_list.append(crop)
					# print len(falses_positives_list), crop.shape
	return falses_positives_list


def downloadImages(FILE,OUTPUT_DIR):
	interv = int(input("Ingrese el intervalo de imagenes: "))
	n = 0
	links = open(FILE, 'r')
	ImageList=[]
	p = Pool(30)

	for line in links:
		if n % interv==0:
			url= "".join(line.split()[1:])
			image_path = OUTPUT_DIR + 'img' + str(n) + '.jpg'
			ImageList.append([url,image_path])
		n +=1 

	links.close()
	print len(ImageList)

	p.map(getImageRequest,ImageList)

def showFakes(path):
	array = np.load(path)
	for img in array:
		cv2.imshow('img',img)
		cv2.waitKey(0)

if __name__ == "__main__":
	FILE = '/home/ralvarez/Documentos/ImageNet/fall11_urls.txt'

	OUTPUT_DIR = '/home/ssi_ralvarez/Documentos/all_project/RepositoriosExternos/train-detector/raw_neg2/'
	# OUTPUT_DIR = '/home/ralvarez/Documentos/RepositoriosExternos/train-detector/fake/'	
	# showFakes('/home/ssi_ralvarez/Documentos/DLVENV/lpr/fp.npy')
	# platedetector = cv2.CascadeClassifier('co5.xml')
	
	# fp_list=getfp(OUTPUT_DIR,platedetector)
	# np.save('fp.npy',fp_list)

	scan_all_images(OUTPUT_DIR)
	# downloadImages(FILE,OUTPUT_DIR)
	 
	


