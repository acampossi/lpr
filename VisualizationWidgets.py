#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
from  PyQt4 import QtGui,QtCore

# import vispy
# print vispy.__file__, "HOLAAAAAAAA"
from vispy import scene
from vispy.util.quaternion import Quaternion
import numpy as np
import cv2

import numpy as np
import vispy
from vispy import gloo
import os
import logging

import copy

vispy.use('pyqt4')
VW_BASEDIR = os.path.dirname(os.path.abspath(__file__))

vertex = """
    attribute vec2 position;
    attribute vec2 texcoord;
    varying vec2 v_texcoord;
    void main()
    {
        gl_Position = vec4(position, 0.0, 1.0);
        v_texcoord = texcoord;
    }
"""

fragment = """
    uniform sampler2D texture;
    varying vec2 v_texcoord;
    void main()
    {
        gl_FragColor = texture2D(texture, v_texcoord);
        // HACK: the image is in BGR instead of RGB.
        float temp = gl_FragColor.r;
        gl_FragColor.r = gl_FragColor.b;
        gl_FragColor.b = temp;
    }
"""
BUTTON_SIZE = 36
TOOT_TIP_STYLESHEET = "QToolTip { color: #000000; background-color: #ffffff; border: 1px  #f0f0f0; }"

class QPlainTextEditLogger(logging.Handler):
    def __init__(self, parent):
        super(QPlainTextEditLogger,self).__init__()
        self.widget = QtGui.QPlainTextEdit(parent)
        self.widget.setReadOnly(True)    
        self.setFormatter(logging.Formatter('%(asctime)s - %(levelname)s - %(message)s'))

    def emit(self, record):
        msg = self.format(record)
        self.widget.appendPlainText(msg)    

class TextAndSpinBox(QtGui.QWidget):

    def __init__(self,text,spinbox,min=0,max=0):
        super(TextAndSpinBox, self).__init__()
        layout = QtGui.QHBoxLayout()
        text = QtGui.QLabel(text)
        layout.addWidget(text)
        layout.addWidget(spinbox)
        spinbox.setMinimum(min)
        spinbox.setMaximum(max)
        self.setLayout(layout)        

class RadButtonandslider(QtGui.QButtonGroup):

    def __init__(self,label,radiobuttonlist,slider,sliderLabel):
        super(RadButtonandslider,self).__init__()
        
        self.widget = QtGui.QGroupBox(label)
        layout = QtGui.QVBoxLayout()
        layout.setSpacing(0)
        layout.setMargin(0)
        buttonlist = QtGui.QGroupBox("Tipo de ajuste:")
        b_layout =QtGui.QHBoxLayout()
        #b_layout.setSpacing(0)
        self.buttonList = []
        cnt = 0
        for label in radiobuttonlist:
            button =QtGui.QRadioButton(label)
            self.addButton(button,cnt)
            cnt +=1
            self.buttonList.append(button)
            b_layout.addWidget(button)
        
        buttonlist.setLayout(b_layout)
        layout.addWidget(buttonlist)
        layout.addWidget(QtGui.QLabel(sliderLabel))
        layout.addWidget(slider)
        self.widget.setLayout(layout)


class HelpWindow(QtGui.QWidget):
    def __init__(self,imagepath, parent=None):
        super(HelpWindow, self).__init__()
        self.pic1 = QtGui.QLabel()
        imagepath = os.path.abspath(imagepath)
        im = QtGui.QPixmap(imagepath)
        self.pic1.setPixmap(im)
        layout = QtGui.QHBoxLayout()
        layout.addWidget(self.pic1)
        self.setLayout(layout)
        self.setWindowTitle(u"Ayuda...")
        stylesheet = "background: white"
        self.setStyleSheet(stylesheet)
        self.move(30,30)
            


class ResetButton(QtGui.QPushButton):
    def __init__(self, parent=None,tooltip=None):
        super(ResetButton, self).__init__('')
        if tooltip != None:
                self.setToolTip(tooltip)
        
        stylesheet =  "QPushButton{border-style: solid;}" +TOOT_TIP_STYLESHEET
        
        self.setStyleSheet(stylesheet)        
        self.setFocusPolicy(QtCore.Qt.NoFocus)
        self.setIcon(QtGui.QIcon(VW_BASEDIR+u'/recargar_v2.jpg'))
        self.setIconSize(QtCore.QSize(BUTTON_SIZE,BUTTON_SIZE))

class HelpButton(QtGui.QPushButton):
    def __init__(self, parent=None,tooltip=None,):
        super(HelpButton, self).__init__('')
        if tooltip != None:
                self.setToolTip(tooltip)
        
        stylesheet =  "QPushButton{border-style: solid;}" +TOOT_TIP_STYLESHEET
        
        self.setStyleSheet(stylesheet)        
        self.setFocusPolicy(QtCore.Qt.NoFocus)
        self.setIcon(QtGui.QIcon(VW_BASEDIR+u'/icons8-Help-520.png'))
        self.setIconSize(QtCore.QSize(BUTTON_SIZE,BUTTON_SIZE))


class SearchButton(QtGui.QPushButton):
    def __init__(self, parent=None):
        super(SearchButton, self).__init__('')

        stylesheet = """
        border-style: solid;
        """
        self.setStyleSheet(stylesheet)        

        self.setFocusPolicy(QtCore.Qt.NoFocus)
        self.setIcon(QtGui.QIcon(VW_BASEDIR+u'/lupa.png'))
        self.setIconSize(QtCore.QSize(BUTTON_SIZE,BUTTON_SIZE))

class CarButton(QtGui.QPushButton):
    def __init__(self, parent=None):
        super(CarButton, self).__init__('')

        stylesheet = """
        border-style: solid;
        """
        self.setStyleSheet(stylesheet)        

        self.setFocusPolicy(QtCore.Qt.NoFocus)
        self.setIcon(QtGui.QIcon(VW_BASEDIR+u'/car.jpg'))
        self.setIconSize(QtCore.QSize(80,80))

class CameraButton(QtGui.QPushButton):
    def __init__(self, parent=None):
        super(CameraButton, self).__init__('')

        stylesheet = """
        border-style: solid;
        """
        self.setStyleSheet(stylesheet)        

        self.setFocusPolicy(QtCore.Qt.NoFocus)
        self.setIcon(QtGui.QIcon(VW_BASEDIR+u'/camera.png'))
        self.setIconSize(QtCore.QSize(80,80))


class SearchWidget(QtGui.QWidget):

    def __init__(self,qtextedit,searchButton):
        super(SearchWidget, self).__init__()
        qtextedit.setMaximumHeight(BUTTON_SIZE)
        layout =QtGui.QHBoxLayout()
        layout.addWidget(qtextedit)
        layout.addWidget(searchButton)
        self.setLayout(layout)
        
        

class MyButton(QtGui.QPushButton):
    def __init__(self,label, parent=None,tooltip=None):
        
        if label =="Siguientes":
            super(MyButton, self).__init__()
            BUTTON_SIZE =70
            stylesheet =  "QPushButton{border-style: solid;}" +TOOT_TIP_STYLESHEET
            self.setStyleSheet(stylesheet)        
            self.setFocusPolicy(QtCore.Qt.NoFocus)
            self.setIcon(QtGui.QIcon(VW_BASEDIR+u'/check.png'))
            self.setIconSize(QtCore.QSize(BUTTON_SIZE,BUTTON_SIZE))
            #self.setMinimumSize(BUTTON_SIZE,BUTTON_SIZE)
            
        else:
            super(MyButton, self).__init__(label)
            self.setFocusPolicy(QtCore.Qt.NoFocus)
            stylesheet = """QPushButton { Background:rgb(33,150,243);
                        color:white;
                        border-color:rgb(33,150,243);
                        border-style: solid;
                        border-width: 20;

                        font-size:20px;}""" +TOOT_TIP_STYLESHEET

            self.setStyleSheet(stylesheet)        
            self.setMinimumSize(150,70)
            self.setMaximumSize(150,70)
            
            if label =="Siguiente":
                self.setText(u"✔")
                stylesheet = """QPushButton {Background:rgb(33,150,243);
                        color:white;
                        border-color:rgb(33,150,243);
                        border-style: solid;
                        border-width: 20;
                        font-size:40px;}""" +TOOT_TIP_STYLESHEET

                self.setStyleSheet(stylesheet)        
        if tooltip != None:
            self.setToolTip(tooltip)

class ViewButton(QtGui.QPushButton):
    def __init__(self,label,canvas,image, parent=None,tooltip=None):
        super(ViewButton, self).__init__(label)
        self.setFocusPolicy(QtCore.Qt.NoFocus)
        stylesheet = """QPushButton { Background:rgb(33,150,243);
                    color:white;
                    border-color:rgb(33,150,243);
                    border-style: solid;
                    border-width: 20;

                    font-size:16px;}""" +TOOT_TIP_STYLESHEET

        self.setStyleSheet(stylesheet)        
      
        self.image = image
        self.canvas = canvas
        self.clicked.connect(lambda: self.canvas.showImage(self.image))
    
    
    


class LicensePLateTable(QtGui.QTableWidget):
    # Class_list = [u"No Vehiculos",
    #             u"Automóviles, camperos y camionetas",
    #             u"Microbuses con llanta sencilla en el eje trasero",
    #             u"Busetas, Buses, Microbuses con doble llanta trasera",
    #             u"Camiones de mas de dos ejes",
    #             u"Camiones  de 2 ejes",
    #             ]
    

    def __init__(self,rows,colums, parent=None,rowHeight=50):
        super(LicensePLateTable, self).__init__(rows,colums)
        stylesheet = """::section{Background-color:rgb(33,150,243);
                     color:white;
                     border-color:beige;
                      border-style: solid;
                      font-size:14px}"""
        
        self.RowNum =0
        self.horizontalHeader().setStyleSheet(stylesheet)
        self.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)
        self.verticalHeader().setVisible(False)
        self.setWindowFlags(self.windowFlags() | QtCore.Qt.FramelessWindowHint)
        
        self.setFocusPolicy(QtCore.Qt.NoFocus)
        self.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)
        self.setSelectionMode(QtGui.QAbstractItemView.NoSelection)
        
        #self.horizontalHeader().setVisible(False)
        self.setHorizontalHeaderLabels(['Placa','Hora Ingreso','Hora Salida'])
        header = self.horizontalHeader()
        header.setResizeMode(0, QtGui.QHeaderView.Stretch)
        #header.setResizeMode(1, QtGui.QHeaderView.ResizeToContents)
        # self.setColumnWidth(1,rowHeight)
        self.setColumnWidth(0,200)
        self.setColumnWidth(1,300)
        # self.setColumnWidth(2,300)
        
    def changeValue(self,row,column,value):
        self.setItem(row, column, QtGui.QTableWidgetItem(value))

    def InsertNewRow(self):
        self.insertColumn(self.RowNum)
        self.RowNum += 1

    def cellClicked(self,a,b):
        print a,b

class Mytable(QtGui.QTableWidget):
    # Class_list = [u"No Vehiculos",
    #             u"Automóviles, camperos y camionetas",
    #             u"Microbuses con llanta sencilla en el eje trasero",
    #             u"Busetas, Buses, Microbuses con doble llanta trasera",
    #             u"Camiones de mas de dos ejes",
    #             u"Camiones  de 2 ejes",
    #             ]
    Class_list = [u"No Vehiculos",
                u"Automóviles, camperos y camionetas",
                u"Microbuses con llanta sencilla en el eje trasero",
                u"Busetas, Buses, Microbuses con doble llanta trasera",
                u"Camiones de 2 ejes",
                u"Vehículos de 3 ejes (de carga o pasajeros)",
                u"Vehículos de 4 ejes (de carga o pasajeros)",
                u"Vehículos de 5 ejes (Camión o vehículo de carga)",
                u"Vehículos de 6 ejes (Camión o vehículo de carga)",
                ]

    def __init__(self,rows,colums, parent=None):
        super(Mytable, self).__init__(rows,colums)
        stylesheet = """::section{Background-color:rgb(33,150,243);
                     color:white;
                     border-color:beige;
                      border-style: solid;
                      font-size:14px}"""
        
        self.horizontalHeader().setStyleSheet(stylesheet)
        self.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)
        self.verticalHeader().setVisible(False)
        self.setWindowFlags(self.windowFlags() | QtCore.Qt.FramelessWindowHint)
        
        self.setFocusPolicy(QtCore.Qt.NoFocus)
        self.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)
        self.setSelectionMode(QtGui.QAbstractItemView.NoSelection)
        
        #self.horizontalHeader().setVisible(False)
        self.setHorizontalHeaderLabels(['Clase','C. Izquierdo','C. Derecho'])
        header = self.horizontalHeader()
        header.setResizeMode(0, QtGui.QHeaderView.Stretch)
        #header.setResizeMode(1, QtGui.QHeaderView.ResizeToContents)
        self.setColumnWidth(1,100)
        # self.setColumnWidth(0,350)
        # self.setColumnWidth(1,146)
        
        # for i in range(9):
        #     self.setRowHeight(i,100)
        #     self.setItem(i, 0, QtGui.QTableWidgetItem(self.Class_list[i]))

    def changeValue(self,row,column,value):
        self.setItem(row, column, QtGui.QTableWidgetItem(value))

    def GetValue(self,row,column):
        return self.getItem(row, column)


class MyQComboBox(QtGui.QComboBox):
    
    def __init__(self,tooltip =None):
        super(MyQComboBox,self).__init__()
        self.setStyleSheet(TOOT_TIP_STYLESHEET)
        if tooltip != None:
            self.setToolTip(tooltip)
        
class MyQSpinBox(QtGui.QSpinBox):
    
    def __init__(self,tooltip =None):
        super(MyQSpinBox,self).__init__()
        self.setStyleSheet(TOOT_TIP_STYLESHEET)
        if tooltip != None:
            self.setToolTip(tooltip)
        
class MyQCheckBox(QtGui.QCheckBox):
    
    def __init__(self,text,parent=None,tooltip =None):
        super(MyQCheckBox,self).__init__(text,parent=parent)
        self.setStyleSheet(TOOT_TIP_STYLESHEET)
        if tooltip != None:
            self.setToolTip(tooltip)
        
class ConfigurationWidget(QtGui.QGroupBox):
    
    signal_objet_changed = QtCore.pyqtSignal(name='objectChanged')
    gbox = None
    def __init__(self,label=u"Panel de Control",maxWidth=300):
        
        super(ConfigurationWidget, self).__init__(label)
        if maxWidth!= None:
            self.setMaximumWidth(maxWidth)
    
        self.gbox = QtGui.QFormLayout()
        self.gbox.setSpacing(0)

        vbox = QtGui.QVBoxLayout()
        vbox.addLayout(self.gbox)
        vbox.addStretch(1.0)
        # se adiciona el boton de reset        
        self.resetButton = ResetButton()
        self.helpButton = HelpButton()
        ResetButton_l =QtGui.QHBoxLayout()
        ResetButton_l.addStretch(1)
        ResetButton_l.addWidget(self.helpButton)
        ResetButton_l.addWidget(self.resetButton)
        vbox.addLayout(ResetButton_l)
        self.setLayout(vbox)
    

    def update_param(self, option):
        self.signal_objet_changed.emit()



class PlotDataWidget(scene.SceneCanvas):
    def __init__(self,Title='',Xaxis='',Yaxis=''):
        scene.SceneCanvas.__init__(self,keys='interactive', size=(600, 600), show=True,bgcolor=(1,1,1,1))

        self.unfreeze()
        self.grid = self.central_widget.add_grid(margin=10)
        self.plot_dic = {}
        
        title = scene.Label(Title, color='black')
        title.height_max = 40
        self.grid.add_widget(title, row=0, col=0, col_span=2)

    
        self.yaxis = scene.AxisWidget(orientation='left',
                                axis_label='Y Axis',
                                axis_font_size=12,
                                axis_label_margin=50,
                                tick_label_margin=5,
                                text_color='black')
        self.yaxis.width_max = 80
        self.grid.add_widget(self.yaxis, row=1, col=0)

        self.xaxis = scene.AxisWidget(orientation='bottom',
                                axis_label='X Axis',
                                axis_font_size=12,
                                axis_label_margin=50,
                                tick_label_margin=5,
                                text_color='black')

        self.xaxis.height_max = 80
        self.grid.add_widget(self.xaxis, row=2, col=1)

        right_padding = self.grid.add_widget(row=1, col=2, row_span=1)
        right_padding.width_max = 50

        self.view = self.grid.add_view(row=1, col=1, border_color='black')

        data = np.arange(20).reshape((10,2))
        self.linegrid = scene.visuals.GridLines( parent= self.view.scene,color=(0,0,0,0.5))
        self.linegrid.set_gl_state('translucent', depth_test=False)
        
        
        self.view.camera =  scene.PanZoomCamera(rect=(0,0, 10, 10))
        self.xaxis.link_view(self.view)
        self.yaxis.link_view(self.view)
    
    
    def CreateLine(self,key,x,y,color=(1,0,0,1)):
        l =x.shape[0]
        pos = np.empty((l, 2), dtype=np.float32)
        pos[:,0] =x
        pos[:,1] =y
        if key in self.plot_dic:
            self.plot_dic[key].set_data(pos)
        else:
            plotter = scene.Line(pos,color= color, parent=self.view.scene)
            self.plot_dic[key] =plotter
        self.update()

    def CreatePolygon(self,key,x,y,color=(1,0,0,1)):
        l =x.shape[0]
        pos = np.empty((l, 2), dtype=np.float32)
        pos[:,0] =x
        pos[:,1] =y
        if key in self.plot_dic:
            self.plot_dic[key].pos =pos
        else:
            plotter = scene.visuals.Polygon(pos=pos, color=color,border_color=color,parent=self.view.scene)
            self.plot_dic[key] =plotter
        self.update()
    def ChangeRec(self,rect):
        self.view.camera.rect=rect
    
    
    def CreateMarkers(self,key,x,y,color=(1,0,0,1)):
        l =x.shape[0]
        pos = np.empty((l, 2), dtype=np.float32)
        pos[:,0] =x
        pos[:,1] =y
        if   key in self.plot_dic:
            self.plot_dic[key].set_data(pos,edge_color=color, face_color=color, size=2)
        else:
            #plotter = scene.Line(pos,color= color, parent=self.view.scene)
            scatter = scene.visuals.Markers()
            self.view.add(scatter)
            self.plot_dic[key] =scatter
            self.plot_dic[key].set_data(pos,edge_color=color, face_color=color, size=2)
        
        self.update()

         

class PointCloudCanvas(scene.SceneCanvas):

    def __init__(self,height = 800,width = 600,bc ='w'):
        scene.SceneCanvas.__init__(self, bgcolor=bc)
        #self.size = height, width
        self.unfreeze()
        self.view = self.central_widget.add_view()
        self.view.camera = 'arcball'
        self.scatter = scene.visuals.Markers()
        scene.cameras.TurntableCamera(azimuth=30.0,elevation=30.0)
        pos=  np.random.randn(1000, 3)
        self.scatter.set_data(pos[:,[2,0,1]], edge_color=(1, 1, 1, 1), face_color=(1, 1, 1, .5), size=5)
        self.view.add(self.scatter)
        scene.visuals.XYZAxis(parent=self.view.scene)


    def set_data(self, pos, colorV=(1, 1, 1, 1),colorF=(1, 1, 1, .5)):
        pos = copy.copy(pos[:,[2, 0, 1]])
        pos[:,2] = -pos[:,2]
        self.scatter.set_data(pos, edge_color=colorV, face_color=colorF, size=2)
        self.update()


class PointArrowsCanvas(scene.SceneCanvas):
    width = 1.0
    Arrowsize = 2.
    def __init__(self,height = 800,width = 600):
        scene.SceneCanvas.__init__(self, bgcolor='k')
        #self.size = height, width
        self.unfreeze()
        self.view = self.central_widget.add_view()
        self.view.camera = 'arcball'
        self.view.camera._quaternion = Quaternion(w=-0.54,x=-0.493,y=0.489,z=-0.476)

        scene.cameras.TurntableCamera(azimuth=30.0,elevation=30.0)
  
        self.scatter = scene.visuals.Arrow(connect='segments',
                     arrow_type='triangle_60', arrow_size=self.Arrowsize,
                     width=self.width, antialias=True,arrow_color='w', color='w')
        
        self.view.add(self.scatter)
        scene.visuals.XYZAxis(parent=self.view.scene)     

    def set_data(self, data):
        arrows =copy.copy(data[::5,:])
        arrows[:,2] = -arrows[:,2]
        arrows[:,3:] =arrows[:,3:]*0.1 +arrows[:,:3]
        pos = arrows.reshape((arrows.shape[0]*2,3))
        self.scatter.set_data(pos=pos, color='w', width=self.width, connect='segments', arrows=arrows)

class PointCloudCanvasGrid(QtGui.QWidget):
    def __init__(self):
        super(PointCloudCanvasGrid,self).__init__()
        self.size = 800,800
        self.grid1 =PointCloudCanvas(500,500,bc ='k')
        self.grid1.create_native()
        self.grid2 =PointCloudCanvas(500,500,bc ='k')
        self.grid2.create_native()
        self.grid3 =PointCloudCanvas(500,500,bc ='k')
        self.grid3.create_native()
        self.grid4 =PointArrowsCanvas(500,500)
        self.grid4.create_native()
        layout = QtGui.QFormLayout()
        layout.addRow(self.grid1.native,self.grid2.native)
        layout.addRow(self.grid3.native,self.grid4.native)
        
        # layout.addWidget(self.grid1,0,0)
        # layout.addWidget(self.grid2,0,1)
        # layout.addWidget(self.grid3,1,0)
        # layout.addWidget(self.grid4,1,1)
        
        self.setLayout(layout)

class ClosingMessage(QtGui.QMessageBox):
    GUARDAR = 'Guardar'
    NO_GUARDAR  ='No Guardar'
    CANCELAR = 'Cancelar'
    def __init__(self):
        super(ClosingMessage, self).__init__()
        self.setIcon(QtGui.QMessageBox.Question)
        self.setWindowTitle('Confirmar Cierre...')
        self.setText(u'Desea almacenar  los ultimos parametros de configuración?')
        self.setStandardButtons(QtGui.QMessageBox.Save | QtGui.QMessageBox.Discard | QtGui.QMessageBox.Cancel)
        buttonY = self.button(QtGui.QMessageBox.Save)
        buttonY.setText(self.GUARDAR)
        buttonN = self.button(QtGui.QMessageBox.Discard)
        buttonN.setText(self.NO_GUARDAR)
        buttonD = self.button(QtGui.QMessageBox.Cancel)
        buttonD.setText(self.CANCELAR)
        
        self.exec_()
        self.result = None
        if self.clickedButton() == buttonY:
            self.result = self.GUARDAR
        elif self.clickedButton() == buttonN:
            self.result = self.NO_GUARDAR
        elif self.clickedButton() ==buttonD:
            self.result = self.CANCELAR



class SelectDevice(QtGui.QMessageBox):
    
    def __init__(self,lasers):
        super(SelectDevice, self).__init__()
        self.setIcon(QtGui.QMessageBox.Question)
        self.setWindowTitle(u'Más de un láser encontrado...')
        self.setText(u'Seleccione el láser a utilizar:')
        for laser in lasers:
            self.addButton(laser[0]+" "+laser[1],QtGui.QMessageBox.ActionRole)
        self.exec_()
        self.selection = [str(self.clickedButton().text()).split()]
        
        # self.result = None
        # if self.clickedButton() == buttonY:
        #     self.result = self.GUARDAR
        # elif self.clickedButton() == buttonN:
        #     self.result = self.NO_GUARDAR
        # elif self.clickedButton() ==buttonD:
        #     self.result = self.CANCELAR





class CameraCanvas(scene.SceneCanvas):
    def __init__(self):
        scene.SceneCanvas.__init__(self, bgcolor='w',size = (640, 480),keys='interactive')
        

        self.unfreeze()
        self.view = self.central_widget.add_view()
        self.view.camera = scene.PanZoomCamera(aspect=1)
        self.view.camera.flip = (0, 1, 0)
        self.view.camera.set_range()
        self.image = scene.visuals.Image()
        self.view.add(self.image)
        
        
    def showImage(self, image):
        self.image.set_data(image[:,:,::-1])
        self.update()

if  __name__ =="__main__":
    appQt = QtGui.QApplication(sys.argv)
    appQt.setStyle("plastique")
    win =  CameraCanvas() #PointArrowsCanvas()#
    win.show()
    appQt.exec_()
    
    # import numpy as np
    # from vispy import scene, app
    # from vispy.scene.visuals import Arrow

    # can = scene.SceneCanvas(show=True)
    # wc = can.central_widget.add_view()
    # wc.camera = 'turntable'
    # wc.camera.center = (0., 0., 0.)
    # wc.camera.scale_factor = 3.

    # pos = np.array([[0., 0., 0.], [1., 0., 0.],
    #                 [0., 0., 0.], [0., 1., 0.],
    #                 [0., 0., 0.], [0., 0., 1.],
    #                 ])
    # arrows = np.c_[pos[0::2], pos[1::2]]

    # line_color = ['red', 'red', 'green', 'green', 'blue', 'blue']
    # arrow_color = ['red', 'green', 'blue']

    # arr = Arrow(pos=pos, parent=wc.scene, connect='segments',
    #             arrows=arrows, arrow_type='triangle_60', arrow_size=20.,
    #             width=3., antialias=True, arrow_color=arrow_color,
    #             color=line_color)

    # can.show()
    # app.run()