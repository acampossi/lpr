import numpy as np
import keras
from keras.models import Sequential
from keras.models import Model

from keras.layers import Input,Dense, Dropout, Flatten
from keras.layers import Conv2D,Conv1D, MaxPooling2D,BatchNormalization
from keras.optimizers import SGD
import cv2

import matplotlib.pyplot as plt
def char2label(char):
    num = ord(char)
    if num>= ord('A') and num <= ord('Z'):
        return num - ord('A')
    elif num>= ord('0') and num <= ord('9'):
        return num - ord('0')+26
                

def label2char(label):

    if label<26:
        return chr(label+65)
    else:
        return chr(label+47-25)
                

# data = np.load('plates_v2_images.npy')
# labels = np.load('plates_v2_letters.npy')
data = np.load('created_plates_v2_images.npy')
labels = np.load('created_plates_v2_letters.npy')

print data.shape
print labels.shape


idx = np.argsort(np.random.randn(labels.shape[0]))
data = data[idx,:,:]
labels = labels[idx]


# for i in range(100):
#     cv2.namedWindow(labels[i],cv2.WINDOW_NORMAL)
#     cv2.resizeWindow(labels[i], 600,600)
#     cv2.imshow(labels[i],data[i,:,:])
#     cv2.waitKey(0)
    
Y = np.zeros((labels.shape[0],6),int)
for j in range(6):
    for  i in range(labels.shape[0]):
        labels[i] = labels[i].upper()
        Y[i,j] = char2label(labels[i][j])
    print labels[i],Y[i,:]
    #plt.hist(Y[:,j].reshape((-1,1)),bins=range(37))
    #plt.show()



# print Y[i,:]
# plt.hist(Y)
# plt.show()

valsize= int(0.1 *labels.shape[0]) 

x_train = data
y1 = keras.utils.to_categorical(Y[:,0], num_classes=36)
y2 = keras.utils.to_categorical(Y[:,1], num_classes=36)
y3 = keras.utils.to_categorical(Y[:,2], num_classes=36)
y4 = keras.utils.to_categorical(Y[:,3], num_classes=36)
y5 = keras.utils.to_categorical(Y[:,4], num_classes=36)
y6 = keras.utils.to_categorical(Y[:,5], num_classes=36)

# x_test =  data[-valsize:,:,:,:]
# y_test = keras.utils.to_categorical(Y[-valsize:,0], num_classes=36)
# print valsize,x_train.shape,y_train.shape


# Headline input: meant to receive sequences of 100 integers, between 1 and 10000.
# Note that we can name any layer by passing it a "name" argument.


def modelGraph():
    w = 96
    h = 48
    c= 1
    input_img = Input(shape=(h, w,c),name='input_img')
    x = Conv2D(32, (3, 3), activation='relu',padding='same')(input_img)
    x =BatchNormalization()(x)
    x = MaxPooling2D(pool_size=(2, 2))(x)
    x = Conv2D(32, (3, 3), activation='relu',padding='same')(x)
    x =BatchNormalization()(x)
    x = MaxPooling2D(pool_size=(2, 2))(x)
    x = Conv2D(32, (3, 3), activation='relu',padding='same')(x)
    x =BatchNormalization()(x)
    x = MaxPooling2D(pool_size=(2, 2))(x)
    x = Conv2D(32, (3, 3), activation='relu',padding='same')(x)
    x =BatchNormalization()(x)
    x = MaxPooling2D(pool_size=(2, 2))(x)
    x = Flatten()(x)

    d1 = Dense(64, activation='relu')(x)    
    d1 =BatchNormalization()(d1)
    d1 = Dropout(0.2)(d1)
    out1 = Dense(36, activation='softmax',name='out1')(d1)

    d2 = Dense(64, activation='relu')(x)
    d2 =BatchNormalization()(d2)
    d2 = Dropout(0.2)(d2)
    out2 = Dense(36, activation='softmax',name='out2')(d2)

    d3 = Dense(64, activation='relu')(x)
    d3 =BatchNormalization()(d3)
    d3 = Dropout(0.2)(d3)
    out3 = Dense(36, activation='softmax',name='out3')(d3)

    d4 = Dense(64, activation='relu')(x)
    d4 =BatchNormalization()(d4)
    d4 = Dropout(0.2)(d4)
    out4 = Dense(36, activation='softmax',name='out4')(d4)

    d5 = Dense(64, activation='relu')(x)
    d5 =BatchNormalization()(d5)
    d5 = Dropout(0.2)(d5)
    out5 = Dense(36, activation='softmax',name='out5')(d5)

    d6 = Dense(64, activation='relu')(x)
    d6 =BatchNormalization()(d6)
    d6 = Dropout(0.2)(d6)
    out6 = Dense(36, activation='softmax',name='out6')(d6)


    model = Model(inputs=[input_img], outputs=[out1,out2,out3,out4,out5,out6])


    sgd = SGD(lr=0.001, decay=1e-6, momentum=0.99, nesterov=True)
    model.compile(loss='categorical_crossentropy', optimizer=sgd, metrics=['accuracy'],loss_weights=[1., 1., 1., 1., 1., 1.])
    return model
# model.compile(optimizer=sgd,
#               loss={'main_output': 'binary_crossentropy', 'aux_output': 'binary_crossentropy'},
#               loss_weights={'main_output': 1., 'aux_output': 0.2})

# And trained it via:
data = data.reshape((-1,48,96,1))
# model = modelGraph()
model = keras.models.load_model('/home/ssi_ralvarez/Documentos/DLVENV/lpr/PlatesReaders/my_model9.h5') 
for i in  range(30):
    print i 
    model.fit({'input_img': data},
            {'out1': y1,
            'out2': y2,
            'out3': y3,
            'out4': y4,
            'out5': y5,
            'out6': y6},
            epochs=3, batch_size=64)

    model.save('/home/ssi_ralvarez/Documentos/DLVENV/lpr/PlatesReaders/my_model10.h5')

    