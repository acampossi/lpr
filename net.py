import numpy as np
import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras.optimizers import SGD
import cv2

import matplotlib.pyplot as plt
def char2label(char):
    num = ord(char)
    if num>= ord('A') and num <= ord('Z'):
        return num - ord('A')
    elif num>= ord('0') and num <= ord('9'):
        return num - ord('0')+26
                

def label2char(label):

    if label<26:
        return chr(label+65)
    else:
        return chr(label+47-25)
                

data = np.load('data_images.npy')
labels = np.load('data_letters.npy')
print data.shape
print labels.shape

# for i in range(100):
#     cv2.imshow('im',data[i,:,:,:])
#     print labels[i]
#     cv2.waitKey(0)
Y = np.zeros((labels.shape[0],1),int)
for  i in range(labels.shape[0]):
    print i, labels[i]
    
    Y[i,0] = char2label(labels[i])
    # print Y[i,0]
# plt.hist(Y)
# plt.show()


# Generate dummy data
valsize= int(0.1 *labels.shape[0]) 
print valsize
x_train = data[:-valsize,:,:,:]
y_train = keras.utils.to_categorical(Y[:-valsize,0], num_classes=36)
x_test =  data[-valsize:,:,:,:]
y_test = keras.utils.to_categorical(Y[-valsize:,0], num_classes=36)
print valsize,x_train.shape,y_train.shape

model = Sequential()
# input: 100x100 images with 3 channels -> (100, 100, 3) tensors.
# this applies 32 convolution filters of size 3x3 each.
model.add(Conv2D(32, (3, 3), activation='relu',padding='same', input_shape=(32, 16, 3)))
# model.add(Conv2D(32, (3, 3), activation='relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
# model.add(Dropout(0.25))

model.add(Conv2D(64, (3, 3), activation='relu',padding='same'))
# model.add(Conv2D(64, (3, 3), activation='relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
# model.add(Dropout(0.25))

model.add(Conv2D(64, (3, 3), activation='relu',padding='same'))
# model.add(Conv2D(64, (3, 3), activation='relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Flatten())
model.add(Dense(64, activation='relu'))
model.add(Dropout(0.2))
model.add(Dense(36, activation='softmax'))

sgd = SGD(lr=0.0001, decay=1e-6, momentum=0.9, nesterov=True)
model.compile(loss='categorical_crossentropy', optimizer=sgd, metrics=['accuracy'])

model.fit(x_train, y_train, batch_size=32, epochs=100)
score = model.evaluate(x_test, y_test, batch_size=32)
result = model.predict(x_test, batch_size=32)
print score
# for i in range(36):
#     print  result[i,0],y_test[i,0]
# from sklearn.metrics import accuracy_score
# print accuracy_score(result, y_test)
# print score
# model.save('my_model.h5')

