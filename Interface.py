import sys
from  PyQt4 import QtGui,QtCore
import numpy as np
import cv2
from VisualizationWidgets import CameraCanvas,MyButton, LicensePLateTable,ViewButton
from vispy.app import Timer
from LPR import LPR
from plateRegister  import PlatesRegister




def showAnnotations(img,result,color =(255,0,0)):
    for key in result:
        x,y,w,h = result[key]
        cv2.rectangle(img,(x,y),(x+w,y+h),color,1)
        # cv2.putText(img,key,(x,y),cv2.FONT_HERSHEY_SIMPLEX,1,color,thickness=2)



class LPRModel:

    def __init__(self):
        self.cap = cv2.VideoCapture(1)
        # # self.cap = cv2.VideoCapture('/home/ssi_ralvarez/Descargas/videoplayback.mp4')
        # # self.cap = cv2.VideoCapture('/home/ssi_ralvarez/Descargas/videoplayback (2).mp4')
        self.cap = cv2.VideoCapture('/home/ssi_ralvarez/Descargas/videoplayback (3).mp4')


        self.LPRecognition = LPR('PlatesDetectors/co5.xml','PlatesReaders/my_model10.h5')
        # self.LPRecognition = LPR('PlatesDetectors/co10.xml','PlatesReaders/my_model10.h5')
        
        self.pr = PlatesRegister(time_thd = 10.)


class LPRController:

    def __init__(self, tab, lprmodel,timerinterval ='auto'):
        # se guarda el modelo y la vista en los atributos de las clases
        self.tab = tab
        self.lprmodel = lprmodel
        self.timerinterval = timerinterval
        self.timer = Timer(interval= timerinterval,connect= self.timerevent)
        self.timer.start()
        # se actualizan los puertos existentes
        
        

    def timerevent(self,event):
        cap = self.lprmodel.cap
        LPRecognition = self.lprmodel.LPRecognition
        pr = self.lprmodel.pr

        ret, frame = cap.read()
        if not  ret:
            return
        h,w,c = frame.shape
        frame = frame [:,:2*w/3,:]
        # frame = frame [:h/2,:w/2,:]
        # results = LPRecognition.RecognizePlatesWithCNN(frame,size=(96,48),show=False,neighbors=5,scale=1.3,tolerance=0.01,min_size=(96,48))
        results = LPRecognition.RecognizePlatesWithCNN(frame,size=(96,48),show=False,neighbors=3,scorethd =0.5,scale=1.3,tolerance=0.0)
                
        results = pr.DetelePlatesInside(results)
        for key in results:
            pr.addnewPlates(key,results[key],frame)

        pr.updatePlateRegister()
        showAnnotations(frame,results,color =(0,0,255))
        # print 1./(time.time()-ini)
        self.tab.cameraCanvas.showImage(frame)
        
        # if pr.AcceptedPlates !=[]:
        #     self.tab.imageCanvas.showImage(pr.AcceptedPlates[-1].image)
        self.updateTable()

    def updateTable(self):
        pr = self.lprmodel.pr
        l =len(pr.AcceptedPlates)
        if l>self.tab.table.rowCount():
            self.tab.table.setRowCount(l)
            self.buttonsList = {}
            for i in  range(l):
                self.tab.table.changeValue(i,0,pr.AcceptedPlates[i].plate)
                self.tab.table.changeValue(i,1,str(pr.AcceptedPlates[i].datetime))
                self.buttonsList[i] = ViewButton('Ver',self.tab.imageCanvas,pr.AcceptedPlates[i].image)
                self.tab.table.setCellWidget(i,2,self.buttonsList[i])
        else:
            for i in  range(l):
                self.tab.table.changeValue(i,0,pr.AcceptedPlates[i].plate)
                self.tab.table.changeValue(i,1,str(pr.AcceptedPlates[i].datetime))
                self.buttonsList[i].image = pr.AcceptedPlates[i].image
            

    def showimage(self,i):
        pr = self.lprmodel.pr
        self.tab.imageCanvas.showImage(pr.AcceptedPlates[i].image)
        

    def startAdquisition(self):
        self.update_device()
        self.timer.start()

    def stopAdquisition(self):
        self.timer.stop()


class LPRWindow(QtGui.QWidget):
    
    def __init__(self,Controller=None, parent=None,timerinterval=0.025):
        super(LPRWindow, self).__init__(parent)
        
        F_layout = QtGui.QHBoxLayout() 
        self.cameraCanvas = CameraCanvas()
        self.cameraCanvas.create_native()
        self.cameraCanvas.native.setParent(self)
        self.imageCanvas = CameraCanvas()
        self.imageCanvas.create_native()
        self.imageCanvas.native.setParent(self)
        self.table = LicensePLateTable(0,3)
        F_layout.addWidget(self.cameraCanvas.native)
        table_image_l = QtGui.QVBoxLayout() 
        table_image_l.addWidget(self.imageCanvas.native)
        table_image_l.addWidget(self.table)
        F_layout.addLayout(table_image_l)
        self.Next = MyButton("Iniciar")
        S_layout = QtGui.QHBoxLayout() 
        S_layout.addStretch(1)
        S_layout.addWidget(self.Next)
        V_layout = QtGui.QVBoxLayout()
        V_layout.addLayout(F_layout)
        V_layout.addLayout(S_layout)
        self.setLayout(V_layout)



if  __name__ =="__main__":
    import os
    # os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"   # see issue #152
    # os.environ["CUDA_VISIBLE_DEVICES"] = ""   
    appQt = QtGui.QApplication(sys.argv)
    appQt.setStyle("plastique")
    win =  LPRWindow() #PointArrowsCanvas()#
    model = LPRModel()

    controller = LPRController(win,model)
    win.show()
    appQt.exec_()
    