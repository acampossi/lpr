import time
from datetime import datetime
from Processing_data import CropImage,bb_intersection_over_union,ChangeCoord
import copy
import numpy as np
TIME_THRESHOLD = 10. 



class Plate:

    def __init__(self,plate,pos,img):
        self.plate = plate
        self.charsbuffer= [{},{},{},{},{},{}]
        self.addLetterstodic(plate)
        self.position = pos
        self.n  = 1
        self.time = time.time()
        self.datetime = datetime.now()
        self.IsAdded = False
        self.image = img
        self.platesize = 0

    def addLetterstodic(self,plate):
        newplate = ""
        for i in range(6):
            actual_character = self.plate[i]
            new_character = plate[i]
            if new_character in self.charsbuffer[i]:
                self.charsbuffer[i][new_character] += 1
            else:
                self.charsbuffer[i][new_character] = 1

            if self.charsbuffer[i][new_character] > self.charsbuffer[i][actual_character]:
                newplate = newplate + new_character
            else:
                newplate = newplate +actual_character
        self.plate = newplate
 

 
    def CompareAndUpdate(self,plate,position,img):
        ocr_accuracy = 0.0
        for i in range(6):
            if self.plate[i] == plate[i]:
                ocr_accuracy +=1.
        ocr_accuracy= ocr_accuracy/6
        # if ocr_accuracy + Plate.bb_intersection_over_union(self.position,position)/6  >=0.5:
        if ocr_accuracy  >0.5:
            
            if self.platesize <position[3]:
                self.image = img 
                self.platesize = position[3]
            self.addLetterstodic(plate)
            self.position = position
            self.time =time.time()
            self.datetime = datetime.now()
            self.n =self.n+1
            return True
        else:
            False


    @staticmethod 
    def bb_intersection_over_union(boxA, boxB):
        # determine the (x, y)-coordinates of the intersection rectangle
        xA = max(boxA[0], boxB[0])
        yA = max(boxA[1], boxB[1])
        xB = min(boxA[2]+boxA[0], boxB[2]+boxB[0])
        yB = min(boxA[3]+boxA[1], boxB[3]+boxB[1])
        # compute the area of intersection rectangle
        interArea = 0
        if (xB-xA) >0 and  (yB - yA) >0:
            interArea = (xB - xA + 1) * (yB - yA + 1)

        # compute the area of both the prediction and ground-truth
        # rectangles
        boxAArea = (boxA[2]  + 1) * (boxA[3]  + 1)
        boxBArea = (boxB[2]  + 1) * (boxB[3]  + 1)
    
        # compute the intersection over union by taking the intersection
        # area and dividing it by the sum of prediction + ground-truth
        # areas - the interesection area
        iou = interArea / float(boxAArea + boxBArea - interArea)
    
        # return the intersection over union value
        return iou
class PlatesRegister:

    def __init__(self, time_thd = 5.):
        self.ProcessingPlates = []
        self.AcceptedPlates = []
        self.time_thd = time_thd

    def addnewPlates(self,newplate,position,img):
        finded = False
        for plate in self.ProcessingPlates:
            finded = plate.CompareAndUpdate(newplate,position,img)
            if finded:
                break
        if not finded:
            self.ProcessingPlates.append(Plate(newplate,position,img))

    def updatePlateRegister(self):
        del_list = []
        for i in range(len(self.ProcessingPlates)):
            if time.time() - self.ProcessingPlates[i].time>self.time_thd:
                del_list.append(i)
            if self.ProcessingPlates[i].n >5:
                if not self.ProcessingPlates[i].IsAdded:
                    self.ProcessingPlates[i].IsAdded =True
                    self.AcceptedPlates.append(self.ProcessingPlates[i])
        
        for idx in del_list[::-1]:
            del self.ProcessingPlates[idx]



    def getPlatesLabel(self):
        
        ap = []
        for p in self.AcceptedPlates:
            ap.append([p.plate,p.n])
        return ap







    @staticmethod
    def DetelePlatesInside(results):
        plates, positions = np.asarray(results.keys()),np.asarray(results.values())
        if positions != []:
            idx = np.argsort(positions[:,2])
            plates=plates[idx]
            positions = positions[idx,:]
            positions[:,2:]= positions[:,2:]+positions[:,:2] 
            delete = [True]*positions.shape[0]
            for i in range(positions.shape[0]-1):
                for j in range(i+1,positions.shape[0]):
                    joinA,JoinB = PlatesRegister.JoinedArea( positions[i,:],positions[j,:])
                    if joinA>0.75:
                        if plates[i] in results:
                            del results[plates[i]]
        return results

    @staticmethod
    def JoinedArea(boxA, boxB):
    # determine the (x, y)-coordinates of the intersection rectangle
        xA = max(boxA[0], boxB[0])
        yA = max(boxA[1], boxB[1])
        xB = min(boxA[2], boxB[2])
        yB = min(boxA[3], boxB[3])

        interArea = 0
        if (xB-xA) >0 and  (yB - yA) >0:
            interArea = float((xB - xA + 1) * (yB - yA + 1))

        boxAArea = (boxA[2] - boxA[0] + 1) * (boxA[3] - boxA[1] + 1)
        boxBArea = (boxB[2] - boxB[0] + 1) * (boxB[3] - boxB[1] + 1)


        joined_area_a = interArea/boxAArea
        joined_area_b = interArea/boxBArea

        return joined_area_a,joined_area_b


