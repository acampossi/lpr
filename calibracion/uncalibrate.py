# Python 2/3 compatibility
from __future__ import print_function

import numpy as np
import cv2
import time

# # local modules
# from common import splitfn

# built-in modules
import os

if __name__ == '__main__':
    import sys
    import getopt
    from glob import glob

    camera_matrix = np.loadtxt('camera_matrix.out')
    dist_coefs = np.loadtxt('dist_coefs.out')

    # img_mask = './My_images/*.png'
    img_mask = './Univalle_3/*.png'
    
    img_out = './Univalle_3_output/'
    img_names = glob(img_mask)
    img_names.sort()
    for img_found in img_names:
		img = cv2.imread(img_found)
		infile = img_out+os.path.split(img_found)[1]

		h,  w = img.shape[:2]
		newcameramtx, roi = cv2.getOptimalNewCameraMatrix(camera_matrix, dist_coefs, (w, h), 1, (w, h))
		Inicio = time.time()
		dst = cv2.undistort(img, camera_matrix, dist_coefs, None, newcameramtx)
		print (time.time() - Inicio,  "TIEMPO")
		# crop and save the image
		x, y, w, h = roi
		dst = dst[y:y+h, x:x+w]
		outfile = img_out+os.path.split(img_found)[1] + '_undistorted.png'
		print('Undistorted image written to: %s' % outfile)
		cv2.imwrite(outfile, dst)
		# cv2.imshow('img_2',dst)
		# cv2.waitKey(500)