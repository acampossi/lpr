#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import cv2
import os
import tkFileDialog
import glob
import json
import matplotlib.pyplot as plt
from LPR import LPR,randomPlate
from AugmentorScript import AugmentatorController
from LPR import  showAnnotations
import time
from collections import Counter
import copy
np.set_printoptions(formatter={'float': lambda x: "{0:0.3f}".format(x)})

CROPPED_PLATES_PATH = '/home/ssi_ralvarez/Documentos/Dataset_placas/Cropped_Plates'

ORDERS_LIST =[   [0, 1, 2,3,4,5],
            [0, 2, 1,3,5,4],
            [1, 0, 2,4,3,5],
            [1, 2, 0,4,5,3],
            [2, 0, 1,5,3,4],
            [2, 1, 0,5,4,3]]

NO_ORDERS =[   [0, 1, 2,3,4,5]]


def ChangeCoord(coordenates):
    coordenates = copy.copy(coordenates)
    coordenates[2]+=coordenates[0]
    coordenates[3]+=coordenates[1]
    return coordenates


def ReadJSON(path):
    file_object  = open(path, "r")
    json_info = file_object.read()
    dic = json.loads(json_info)
    file_object.close()
    return dic

def WriteJSON(path,dic):
    file_object = open(path,"w")
    str1 = json.dumps(dic, sort_keys=True,indent=1, separators=(',', ': '))
    # str1 = json.dumps(dic)
    
    file_object.write(str1)
    file_object.close()

def processCharsArray(array):
    if array == 0 or array == None or array == [] or len(array)!=6:
            return [],[]
    characters = []
    letters = []
    for letter in array:
        letters.append(letter.keys()[0]),characters.append( letter.values()[0])
    letters = np.asarray(letters)
    characters = np.asarray(characters)
    x = np.argsort(characters[:,0])
    letters = letters[x]
    characters = characters[x,:]
    return letters,characters


def bb_intersection_over_union(boxA, boxB):
	# determine the (x, y)-coordinates of the intersection rectangle
    xA = max(boxA[0], boxB[0])
    yA = max(boxA[1], boxB[1])
    xB = min(boxA[2], boxB[2])
    yB = min(boxA[3], boxB[3])

    # compute the area of intersection rectangle
    interArea = 0
    if (xB-xA) >0 and  (yB - yA) >0:
        interArea = (xB - xA + 1) * (yB - yA + 1)

	# compute the area of both the prediction and ground-truth
	# rectangles
	boxAArea = (boxA[2] - boxA[0] + 1) * (boxA[3] - boxA[1] + 1)
	boxBArea = (boxB[2] - boxB[0] + 1) * (boxB[3] - boxB[1] + 1)
 
	# compute the intersection over union by taking the intersection
	# area and dividing it by the sum of prediction + ground-truth
	# areas - the interesection area
	iou = interArea / float(boxAArea + boxBArea - interArea)
 
	# return the intersection over union value
	return iou

def organizelabel(label,order):
    out = ""
    for i in range(len(order)):
        out= out+ label[order[i]]
    return out

def CropImage(img,position,tolerance=0.0):
    x_tol = int(tolerance*position[3])
    y_tol = int(tolerance*position[2])
    return img[position[1]-y_tol:position[1]+position[3]+y_tol, position[0]-x_tol:position[0]+position[2]+x_tol,:]


def FindPlate(predict,real):
    cnt = 0
    pos = []
    l = len(real)
    l2 = len(predict)
    for i in range(l):
        for j in range(cnt,l2):
            if real[i] == predict[j]:
                pos.append(j)            
                cnt=j+1
                break
    return pos

def plot_bar_from_counter(counter, ax=None):
    """"
    This function creates a bar plot from a counter.

    :param counter: This is a counter object, a dictionary with the item as the key
     and the frequency as the value
    :param ax: an axis of matplotlib
    :return: the axis wit the object in it
    """

    if ax is None:
        fig = plt.figure()
        ax = fig.add_subplot(111)

    frequencies = counter.values()
    names = counter.keys()

    x_coordinates = np.arange(len(counter))
    ax.bar(x_coordinates, frequencies, align='center')

    ax.xaxis.set_major_locator(plt.FixedLocator(x_coordinates))
    ax.xaxis.set_major_formatter(plt.FixedFormatter(names))

    return ax

class croppingCharacters:


    def __init__(self,augmentor):
        self.W = []
        self.H = []
        self.charsCount = 0		
        self.images_list = []
        self.letters_list = []
        self.position = []
        self.augmentatorcontroller = augmentor
        self.created_images = []
        self.created_labels = []


    def clearList(self):
        self.images_list = []
        self.letters_list = []

    # Cambiar la posicion de los caracteres de acuerdo al orden entregado
    def changeCharactersOrder(self,img,pos,order):
        img_copy = np.copy(img)
    

        for i in range(len(order)):
            if i!=order[i]:
                x,y,w,h = pos[i,:]
                ponderation = self.getimagePonderation(w,h)
                img_copy[y:y+h,x:x+w]= img_copy[y:y+h,x:x+w]*(-ponderation+1.0) +ponderation*cv2.resize(CropImage(img,pos[order[i],:]), dsize=(w,h))

            # img_copy[y:y+h,x:x+w,:]= cv2.resize(CropImage(img,pos[order[i],:]), dsize=(w,h))
        return   img_copy

    # genrea la grilla de  que pondera la imagen nueva con la imagen original 
    def  getimagePonderation(self,w,h):
        image = np.ones((h,w))
        array = [0.3, 0.5, 0.7, 0.9]
        cnt = 0 
        for val in array:
            image[cnt,:] =image[cnt,:]*val
            image[-cnt-1,:] =image[-cnt-1,:]*val
            image[:,cnt] =image[:,cnt]*val
            image[:,-cnt-1] =image[:,-cnt-1]*val
            cnt+=1
        # plt.imshow(image)
        # plt.show()
        return image
    
    # adiciona una posicion al buffer para observar estadisticas
    def GetPositionData(self, position):
        self.position.append(position)

    #  lee las imagenes las recorta y las muestra 
    def CropAndSaveinFile(self,file,position,path):
        img = cv2.imread(file)
        img = CropImage(img,position,tolerance=0.0)
        place,image_name = os.path.split(file)
        _,prefix = os.path.split(place)
        final_path = path+'/'+prefix+'_'+image_name
        print final_path
        cv2.imwrite(final_path,img)


    # lee las imagenes las recorta y las adiciona al buffer
    def CropResizeAndSave(self,file,position,label,size):
        if len(label)==6:
            img = cv2.imread(file)
            img = CropImage(img,position)
            img_resize = cv2.resize(img,dsize=size)
            self.images_list.append(img_resize)
            self.letters_list.append(label)

    #  lee las imagenes las recorta, cambia la posicion de los caracteres y las muestra 
    def CropResizeChangePosAndsave(self,file,position,label,charspos,size= (64,32),show= True,augmentimages= True,tolerance=0.05):

        img = cv2.imread(file)
        letters,letters_pos = processCharsArray(charspos)
        img = CropImage(img,position,tolerance=tolerance)
        

        if len(label)==6:
            if letters_pos != []:        
                orders = ORDERS_LIST
                letters_pos[:,0] = letters_pos[:,0]+int(tolerance*position[3])
                letters_pos[:,1] = letters_pos[:,1]+int(tolerance*position[2])
            else:
                orders = NO_ORDERS

            for order in orders:
                img_resize = self.changeCharactersOrder(img,letters_pos,order)
                temp =self.augmentatorcontroller.GenerateImages(img_resize,shape=size,show= show)
                newlabel = organizelabel(label,order)
                self.images_list = self.images_list +temp
                self.letters_list = self.letters_list + [newlabel]*len(temp)
                self.charsCount+=1
            


    # recorta los caracteres y retorna listas con el resultados y sus etiquetas individuales en listas
    def cropCaracters(self,img,array,show=False):
        if array == 0:
            return []
        characters = []
        letters = []
        for letter in array:
            let, pos = letter.keys()[0],letter.values()[0]
            x,y,w,h = pos
            crop_char_img =  cv2.cvtColor(img[y:y+h,x:x+w,:], cv2.COLOR_BGR2GRAY)
            #crop_char_img = cv2.resize(crop_char_img,dsize=(16,32))
            characters.append(crop_char_img)
            letters.append(let)
            if show:
                cv2.imshow('imagen',crop_char_img )
                cv2.waitKey(0)
        return characters,letters
    
    # recorta los caracteres y los almacena en buffer
    def cropCharactersAndaddtolist(self,img_file,position,array,show=True):
        if array == 0:
            return
        img = cv2.imread(img_file)
        img_crop = CropImage(img,position)
        characters,letters =self.cropCaracters(img_crop,array,show=show)
        # print letters
        self.images_list =self.images_list +characters
        self.letters_list = self.letters_list+letters
        del img

    def GetCharacter(self,char,origin=[]):
        idx = self.letters_list==char
        charlist = self.images_list[idx]                  
        index = int(charlist.shape[0]*np.random.uniform())
        result = charlist[index]

        if origin!=[]:
            max_input = np.amax(result,axis=(0,1))
            min_input = np.amin(result,axis=(0,1))
            max_output = np.amax(origin,axis=(0,1))
            min_output = np.amin(origin,axis=(0,1))
            result =((result-min_input)*((max_output-min_output).astype(float)/(max_input-min_input)) +min_output).astype('uint8')
        return result


    def CreatePlates(self,file,position,label,charspos,size= (64,32),show= False,augmentimages= True,tolerance=0.05):

        img = cv2.imread(file)
        letters,letters_pos = processCharsArray(charspos)
        img = CropImage(img,position,tolerance=tolerance)
        try:
            if len(label)==6 and position!=[]:
                if letters_pos != []:        
                    orders = ORDERS_LIST
                    letters_pos[:,0] = letters_pos[:,0]+int(tolerance*position[3])
                    letters_pos[:,1] = letters_pos[:,1]+int(tolerance*position[2])
                    for i in range(10):
                        newlabel =randomPlate() 
                        img_resize = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
                        img_resize = self.CreatePlate(img_resize,letters_pos,newlabel)
                        temp =self.augmentatorcontroller.GenerateImages(img_resize,shape=size ,show= show)
                        self.created_images = self.created_images +temp
                        self.created_labels = self.created_labels + [newlabel]*len(temp)
            
                else:
                    newlabel=label
                    img_resize = np.copy(img)
                    img_resize = cv2.cvtColor(img_resize, cv2.COLOR_BGR2GRAY)
                    temp =self.augmentatorcontroller.GenerateImages(img_resize,shape=size,show= show)
                    self.created_images = self.created_images +temp
                    self.created_labels = self.created_labels + [newlabel]*len(temp)
        except :
            pass        
        
                    
            
    def CreatePlate(self,img,pos,plate):
        img_copy = np.copy(img)

        
        for i in range(len(plate)):
            x,y,w,h = pos[i,:]
            char = plate[i]
            ponderation = self.getimagePonderation(w,h)
            
            img_copy[y:y+h,x:x+w]= img_copy[y:y+h,x:x+w]*(-ponderation+1.0) +ponderation*cv2.resize(self.GetCharacter(char,origin=img_copy[y:y+h,x:x+w]), dsize=(w,h))
            # img_copy[y:y+h,x:x+w,:]= cv2.resize(CropImage(img,pos[order[i],:]), dsize=(w,h))
            
        # cv2.imshow('img',cv2.cvtColor(img_copy, cv2.COLOR_BGR2GRAY))
        # cv2.waitKey(0)

        return   img_copy


    # guarda la informacion almacenada en el buffer
    def savelist(self,name):
        np.save(name+'_images.npy',np.asarray(self.images_list))
        np.save(name+'_letters.npy',np.asarray(self.letters_list))

    def savecreatedimages(self,name):
        np.save(name+'_images.npy',np.asarray(self.created_images))
        np.save(name+'_letters.npy',np.asarray(self.created_labels))



    def SaveCharacters(self,characterlist,path,suffix='.jpg'):
        for img in characterlist:
            cv2.imwrite(path+'img_'+str(self.charsCount)+ '.jpg', img)
            self.charsCount+=1





EMPTY  = [0, 0, 0, 0]

class Processing_data:

    def __init__(self,cp,JSON_path,folder_path,extension,JSON_char_path=None):

        self.labels_dic = ReadJSON(JSON_path) # diccionario que contiene en
        self.chars_dic  = None
        if JSON_char_path!=None:
            self.chars_dic = ReadJSON(JSON_char_path) # diccionario que contiene en
        
        self.images_path = folder_path[:-1]
        self.ext ='/*'+extension
        garbage,self.prefix = os.path.split(self.images_path)
        self.dir_list =  glob.glob(self.images_path + self.ext)
        self.cp = cp
    
    def getfilename(self,file):
        folder,filename = os.path.split(file)
        return filename            

    def validate_image(self,file):
            plate_label = ""
            filename = self.getfilename(file)
            # se observa si existe una etiqueta
            if filename in self.labels_dic:
                # en caso afirmativo se obtiene la posicion de la placa y se verifica si es valida
                plate_position = self.labels_dic[filename]['Pos_usuario']
                
                if plate_position != EMPTY:
                    
                    if 'Placa_usuario' in self.labels_dic[filename]:
                        plate_label = self.labels_dic[filename]['Placa_usuario']
                        if plate_label!=0 and len(plate_label)==6:
 
                            if self.chars_dic!=None:
                                if filename in self.chars_dic:
                                    return True,plate_position,plate_label,filename,self.chars_dic[filename]
                            
                            return True,plate_position,plate_label,filename,None
                                
            return False,EMPTY,"","",None

    

    def processData(self):        
        #para cada una de las imagenes en la carpeta:
        for file in self.dir_list:
            # se valida la informacion y se obtiene los datos
            success,plate_position,plate_label,filename,chars_position = self.validate_image(file)
            
            if success:
                # print self.cp.charsCount
                # self.cp.GetPositionData(plate_position)
                # self.cp.CropResizeAndShow(file,plate_position,[(128,64),(96,48),(64,32)])
                # self.cp.CropResizeAndSave(file,plate_position,plate_label,(64,32))
                self.cp.CropAndSaveinFile(file,plate_position,CROPPED_PLATES_PATH)
                # self.cp.cropCharactersAndaddtolist(file,plate_position,chars_position,show=False)
                # self.cp.CropResizeChangePosAndsave(file,plate_position,plate_label,chars_position,size= (96,48),show= True)

        # self.cp.ShowPositionStatistics()
    # def CreatePLates(self):        
    #     for file in self.dir_list:
    #         success,plate_position,plate_label,filename,chars_position = self.validate_image(file)
    #         if success:
    #             if chars_position!=None:
    #                 self.cp.cropCharactersAndaddtolist(file,plate_position,chars_position,show=False)
        
    #     self.cp.letters_list = np.asarray(self.cp.letters_list)
    #     self.cp.images_list = np.asarray(self.cp.images_list)
    #     for file in self.dir_list:
    #         success,plate_position,plate_label,filename,chars_position = self.validate_image(file)
    #         if success:
    #             self.cp.CreatePlates(file,plate_position,plate_label,chars_position,size=(96,48),show=True)
        
    #     self.cp.clearList()

    def CreatePLatesWithCroppedCharacters(self,data):        
        # se crea una lista con imagenes de caracteres en blanco y negro
        for file,plate_position,plate_label,filename,chars_position in data:
            if chars_position!=None:
                self.cp.cropCharactersAndaddtolist(file,plate_position,chars_position,show=False)
                print len(self.cp.images_list)
                if len(self.cp.images_list)>15000:
                    break
                
        self.cp.letters_list = np.asarray(self.cp.letters_list)
        self.cp.images_list = np.asarray(self.cp.images_list)
        for file,plate_position,plate_label,filename,chars_position in data:
            self.cp.CreatePlates(file,plate_position,plate_label,chars_position,size=(96,48),show=False)
            print len(self.cp.created_images)
        


    def ValidateRecognition(self,LPRecognition,validator):        
        #para cada una de las imagenes en la carpeta:
        for file in self.dir_list:
            # se valida la informacion y se obtiene los datos
            success,plate_position,plate_label,filename,chars_position = self.validate_image(file)
            if success:
                
                img = cv2.imread(file)
                results = LPRecognition.RecognizePlatesWithCNN(img,size=(96,48),show=False,neighbors=2,scorethd =0.5)        
                Recognized,Detected,falses = validator.validateresult(results,plate_label,plate_position)
                
                print validator.getStatistics(),plate_label
                print validator.fpd,validator.ei
                if  falses!= []:
                    showAnnotations(img,results,plate_position=plate_position,title=plate_label)


                # img = CropImage(cv2.imread(file),plate_position)
                # result = LPRecognition.RecognizeLetters(img,(96,48))
                # Recognized = validator.validateplatetext(result,plate_label.upper())
                # if  not Recognized:
                #     print file,plate_label
                #     cv2.namedWindow(result,cv2.WINDOW_NORMAL)
                #     cv2.resizeWindow(result, 600,600)
                #     cv2.imshow(result,img)
                #     cv2.waitKey(0)
                # print validator.getStatisticstext()
                
                
    def getdata(self,data):        
        for file in self.dir_list:
            success,plate_position,plate_label,filename,chars_position = self.validate_image(file)
            if success:
                data.append([file,plate_position,plate_label,filename,chars_position])
        return data
        
                
              


class validation:
    
    def __init__(self):
        self.tpd = 0
        self.fpd = 0
        self.fnd = 0
        self.tpr = 0
        self.fpr = 0
        self.ei = 0
        self.tprc = np.zeros((6,),dtype=float)
        self.fprc = np.zeros((6,),dtype=float)
        
    def validateresult(self,results,realplate,realposition,thd=0.5,transoformCoord=True):
        

        Recognized = False
        Detected = False
        self.ei += 1
        falses = []
        if transoformCoord:
            realposition = ChangeCoord(realposition)
        for plate,value in results.iteritems():
            if transoformCoord:
                value = ChangeCoord(value)
            iou = bb_intersection_over_union(value,realposition)
            if iou>=thd and iou <=1.:
                if not Detected:
                    self.tpd +=1
    
                Detected = True
                if len(realplate)==6:

                    if plate==realplate:
                        self.tpr+=1
                        Recognized = True
                    else:
                        self.fpr+=1
                    for i in range(6):
                        if plate[i]==realplate[i]:
                            self.tprc[i]+=1
                        else:
                            self.fprc[i]+=1
            else:
                self.fpd += 1
                falses.append(value)
        return Recognized,Detected,falses
    
    def validateplatetext(self,plate,realplate):
            recognized = False
            if plate==realplate:
                self.tpr+=1
                recognized = True
            else:
                self.fpr+=1
            for i in range(6):
                if plate[i]==realplate[i]:
                    self.tprc[i]+=1
                else:
                    self.fprc[i]+=1
            return recognized

    def getStatisticstext(self):
        return  {"plate recognition rate":100.*float(self.tpr)/(float(self.tpr)+float(self.fpr)),
                "char recognition rate":100.*self.tprc/(self.tprc+self.fprc)
                }         


    def getStatistics(self):
        try:

            return  {"plate recognition rate":100.*float(self.tpr)/(float(self.tpr)+float(self.fpr)),
                    "char recognition rate":100.*self.tprc/(self.tprc+self.fprc),
                    "plates detected":100.*float(self.tpd)/float(self.ei),
                    "plates detected precision":100.*float(self.tpd)/(float(self.tpd)+float(self.fpd)),
                    }         
            
        except :
            return



def main():

    folder_path ='/home/ssi_ralvarez/Documentos/Dataset_placas'
    crop_plates_path = folder_path+ "/Cropped_Plates/"
    crop_chars_path = folder_path+ "/Cropped_Chars/"

    files_dic = {
                # "/home/ssi_ralvarez/Documentos/all_project/Repositorio/license_plate_ssi/Registros_placas/registro_placas.txt":["/home/ssi_ralvarez/Documentos/Dataset_placas/Univalle1/",".jpg",None],
                # "/home/ssi_ralvarez/Documentos/all_project/Repositorio/license_plate_ssi/Registros_placas/registro_placas_3.txt":["/home/ssi_ralvarez/Documentos/Dataset_placas/Univalle2/Sn_distorsion/",".png",None],
                # "/home/ssi_ralvarez/Documentos/all_project/Repositorio/license_plate_ssi/Registros_placas/registro_placas_4.txt":["/home/ssi_ralvarez/Documentos/Dataset_placas/Univalle3/",".png","/home/ssi_ralvarez/Documentos/all_project/Repositorio/license_plate_ssi/Registros_placas/json_char(viernes_24).txt"],
                # "/home/ssi_ralvarez/Documentos/all_project/Repositorio/license_plate_ssi/Registros_placas/registro_placas_5.txt":["/home/ssi_ralvarez/Documentos/Dataset_placas/Univalle4/",".png",None],
                "/home/ssi_ralvarez/Documentos/all_project/Repositorio/license_plate_ssi/Registros_placas/registro_placas_6.txt":["/home/ssi_ralvarez/Documentos/Dataset_placas/Univalle5/Sin_Distorcion/",".png","/home/ssi_ralvarez/Documentos/all_project/Repositorio/license_plate_ssi/Registros_placas/json_char(viernes_1).txt"]
                # "/home/ssi_ralvarez/Documentos/all_project/Repositorio/license_plate_ssi/Registros_placas/registro_placas_7.txt":["/home/ssi_ralvarez/Documentos/Dataset_placas/proyecto/PDIPlacas/carros/",".jpg","/home/ssi_ralvarez/Documentos/all_project/Repositorio/license_plate_ssi/Registros_placas/json_char(carros_medellin).txt"],
                # "/home/ssi_ralvarez/Documentos/all_project/Repositorio/license_plate_ssi/Registros_placas/registro_placas_9.txt":["/home/ssi_ralvarez/Documentos/Dataset_placas/Descargadas/",".jpg",None]
                }
    # LPRecognition = LPR('co8.xml','my_model9.h5')#{'plates detected': 72.9, 'char recognition rate': array([98.980, 98.810, 99.490, 100.000, 99.660, 99.575]), 'plates detected precision': 95.14563106796116, 'plate recognition rate': 97.10884353741497} # {'plates detected': 93.5, 'plates detected precision': 87.9, 'plate recognition rate': 97.9}
    # LPRecognition = LPR('co9.xml','my_model9.h5')# {'plates detected': 87.1, 'char recognition rate': array([98.648, 98.648, 99.075, 99.786, 99.573, 99.786]), 'plates detected precision': 91.65035877364645, 'plate recognition rate': 96.79715302491103} {'plates detected': 98.5, 'plates detected precision': 81.06, 'plate recognition rate': 97.0} 
    LPRecognition = LPR('PlatesDetectors/co5.xml','PlatesReaders/my_model10.h5')# {'plates detected': 90.4, 'char recognition rate': array([99.038, 98.764, 98.832, 99.725, 99.313, 99.588]), 'plates detected precision': 90.37864680322781, 'plate recognition rate': 96.7032967032967} {'plates detected': 99.2, 'plates detected precision': 78.91, 'plate recognition rate': 97.42} 
    # LPRecognition = LPR('co5.xml','my_model9.h5')#{'plates detected': 96.4, 'char recognition rate': array([95.962, 97.949, 98.397, 99.551, 99.167, 98.718]), 'plates detected precision': 51.74708818635607, 'plate recognition rate': 92.56410256410257}  {'plates detected': 99.95, 'plates detected precision': 25.497448979591837, 'plate recognition rate': 98.45}
    # LPRecognition = LPR('co11.xml','my_model9.h5')#{'plates detected': 96.4, 'char recognition rate': array([95.962, 97.949, 98.397, 99.551, 99.167, 98.718]), 'plates detected precision': 51.74708818635607, 'plate recognition rate': 92.56410256410257}  {'plates detected': 99.95, 'plates detected precision': 25.497448979591837, 'plate recognition rate': 98.45}

    Validator = validation()
    # files_dic = {'registro_placas_6.txt':[folder_path+'/Univalle5/Sin_Distorcion/','.png','json_char.txt']}
    augcontrol =AugmentatorController('/home/ssi_ralvarez/Documentos/DLVENV/lpr/Test/',3)
    # augcontrol =AugmentatorController('/home/ssi_ralvarez/Documentos/LPR/lpr/Test/',5)


    cp = croppingCharacters(augcontrol)
    data = []
    for file in files_dic:
        print cp.charsCount
        print file
        fol_path,extension,char_path = files_dic[file]
        pd = Processing_data(cp,file,fol_path,extension,JSON_char_path=char_path)
        pd.ValidateRecognition(LPRecognition,Validator)
        # data =pd.getdata(data)
        # pd.processData()
        # pd.CreatePLates()
    # cp.savelist("plates_v3")
    # cp.savecreatedimages("created_plates_v1")
    # WriteJSON('registros.JSON',data)



def main2():
    
    # LPRecognition = LPR('co10.xml','my_model9.h5')# {'plates detected': 90.4, 'char recognition rate': array([99.038, 98.764, 98.832, 99.725, 99.313, 99.588]), 'plates detected precision': 90.37864680322781, 'plate recognition rate': 96.7032967032967} {'plates detected': 99.2, 'plates detected precision': 78.91, 'plate recognition rate': 97.42} 
    data = ReadJSON('registros.JSON')    
    LPRecognition = LPR('co11.xml','my_model9.h5')#{'plates detected': 96.4, 'char recognition rate': array([95.962, 97.949, 98.397, 99.551, 99.167, 98.718]), 'plates detected precision': 51.74708818635607, 'plate recognition rate': 92.56410256410257}  {'plates detected': 99.95, 'plates detected precision': 25.497448979591837, 'plate recognition rate': 98.45}
    cnt = 0
    cnt2 = 0
    for i in range(len(data)):
        
        filepath,plate_position,plate_label,filename,chars_position = data[i]
        if chars_position ==None:
            cnt2 +=1
            img = CropImage(cv2.imread(filepath),plate_position,tolerance=0.0)
            plate,positions = LPRecognition.RecognizePlates(img)
            pos =FindPlate(plate.upper(),plate_label.upper())
            if len(pos)==6:
                positions = positions [pos,:]
                dic =[]
                for j in range(6):
                    x,y,w,h = positions[j]
                    dic.append({plate_label.upper()[j] : [int(x),int(y),int(w),int(h)]})
                    cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),1)
                data[i][4]=dic 

                # if cnt %100==0:
                #     print dic
                #     cv2.imshow(plate,img)
                #     cv2.waitKey(0)
                cnt += 1
        print cnt,cnt2
    WriteJSON('registrosv2.JSON',data)



def main3():

    folder_path ='/home/ssi_ralvarez/Documentos/Dataset_placas'
    crop_plates_path = folder_path+ "/Cropped_Plates/"
    crop_chars_path = folder_path+ "/Cropped_Chars/"
    # LPRecognition = LPR('co8.xml','my_model9.h5')#{'plates detected': 72.9, 'char recognition rate': array([98.980, 98.810, 99.490, 100.000, 99.660, 99.575]), 'plates detected precision': 95.14563106796116, 'plate recognition rate': 97.10884353741497} # {'plates detected': 93.5, 'plates detected precision': 87.9, 'plate recognition rate': 97.9}
    # LPRecognition = LPR('co9.xml','my_model9.h5')# {'plates detected': 87.1, 'char recognition rate': array([98.648, 98.648, 99.075, 99.786, 99.573, 99.786]), 'plates detected precision': 91.65035877364645, 'plate recognition rate': 96.79715302491103} {'plates detected': 98.5, 'plates detected precision': 81.06, 'plate recognition rate': 97.0} 
    # LPRecognition = LPR('co10.xml','my_model9.h5')# {'plates detected': 90.4, 'char recognition rate': array([99.038, 98.764, 98.832, 99.725, 99.313, 99.588]), 'plates detected precision': 90.37864680322781, 'plate recognition rate': 96.7032967032967} {'plates detected': 99.2, 'plates detected precision': 78.91, 'plate recognition rate': 97.42} 
    # LPRecognition = LPR('co5.xml','my_model9.h5')#{'plates detected': 96.4, 'char recognition rate': array([95.962, 97.949, 98.397, 99.551, 99.167, 98.718]), 'plates detected precision': 51.74708818635607, 'plate recognition rate': 92.56410256410257}  {'plates detected': 99.95, 'plates detected precision': 25.497448979591837, 'plate recognition rate': 98.45}
    LPRecognition = LPR('PlatesDetectors/co5.xml','PlatesReaders/my_model9.h5')#{'plates detected': 96.4, 'char recognition rate': array([95.962, 97.949, 98.397, 99.551, 99.167, 98.718]), 'plates detected precision': 51.74708818635607, 'plate recognition rate': 92.56410256410257}  {'plates detected': 99.95, 'plates detected precision': 25.497448979591837, 'plate recognition rate': 98.45}

    # files_dic = {'registro_placas_6.txt':[folder_path+'/Univalle5/Sin_Distorcion/','.png','json_char.txt']}
    augcontrol =AugmentatorController('/home/ssi_ralvarez/Documentos/DLVENV/lpr/Test/',3)
    # augcontrol =AugmentatorController('/home/ssi_ralvarez/Documentos/LPR/lpr/Test/',5)
    
    data = np.asarray(ReadJSON('registrosv2.JSON'))    
    idx = np.argsort(np.random.randn(len(data)))
    data = data[idx]
    print data.shape
    Validator = validation()
    cp = croppingCharacters(augcontrol)
    pd = Processing_data(cp,None,None,'.jpg',JSON_char_path='')
    pd.CreatePLatesWithCroppedCharacters(data)
    cp.savecreatedimages("created_plates_v2")
    # WriteJSON('registros.JSON',data)


if __name__ == "__main__":
    # os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"   # see issue #152
    # os.environ["CUDA_VISIBLE_DEVICES"] = ""   
    main()
    # main3()
# score  0.0  = {'plates detected': 90.51456912585245, 'char recognition rate': array([99.384, 99.658, 99.726, 99.863, 99.795, 99.795]), 'plates detected precision': 90.40247678018576, 'plate recognition rate': 98.4931506849315} IFN268