#!/usr/bin/env python
# -*- coding: utf-8 -*-
import cv2
from LPR import LPR
import time
from plateRegister  import PlatesRegister

def showAnnotations(img,result,color =(255,0,0)):
    for key in result:
        x,y,w,h = result[key]
        cv2.rectangle(img,(x,y),(x+w,y+h),color,1)
        cv2.putText(img,key,(x,y),cv2.FONT_HERSHEY_SIMPLEX,1,color,thickness=2)


TIME_THRESHOLD = 10. 



# cap = cv2.VideoCapture(1)
cap = cv2.VideoCapture('/home/ralvarez/Descargas/videoplayback.mp4')
# cap = cv2.VideoCapture('/home/ssi_ralvarez/Descargas/videoplayback (2).mp4')


LPRecognition = LPR('co9.xml','my_model9.h5')
pr = PlatesRegister(time_thd = 60.)


while(True):
    ini = time.time()    
    ret, frame = cap.read()
    if not  ret:
        break
    h,w,c = frame.shape
    # frame = frame [:,:w/2,:]
    frame = frame [:h/2,:w/2,:]
    
    ini = time.time()
    # results = LPRecognition.RecognizePlatesWithCNN(frame,size=(96,48),show=False,neighbors=5,scale=1.3,tolerance=0.01,min_size=(96,48))
    results = LPRecognition.RecognizePlatesWithCNN(frame,size=(96,48),show=False,neighbors=3,scale=1.1,tolerance=0.01)
            
    results = pr.DetelePlatesInside(results)
    for key in results:
        pr.addnewPlates(key,results[key],frame)

    pr.updatePlateRegister()
    
    print pr.getPlatesLabel()


    showAnnotations(frame,results,color =(0,0,255))
    # print 1./(time.time()-ini)
    cv2.imshow('frame',frame)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

for plate in pr.AcceptedPlates:
    cv2.imshow(plate.plate,plate.image)
    cv2.waitKey(0)

    
    
# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()