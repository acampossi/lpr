import Augmentor
import shutil
import cv2
import glob
class AugmentatorController:

    def __init__(self,folder,num_of_samples,extension='.jpg'):
        self.folder = folder
        self.out_folder = folder+'output'
        self.num_of_samples = num_of_samples
        self.extension = extension

    def GenerateImages(self,img,shape=(96,48),show= True):
        gray = False
        if len(img.shape)<3:
            gray = True
        shutil.rmtree(self.out_folder,ignore_errors=True)
        cv2.imwrite(self.folder + 'temp'+self.extension,img)
        self.ExecuteAugmentorScriptIndependent()
        Filter =self.out_folder+'/*JPEG'
        # print Filter
        files = glob.glob(Filter)
        # print files
        # print files
        imgs = []
        for file in  files:
            image_out = cv2.imread(file)
            image_out =cv2.resize(image_out,dsize=shape)
            if gray:
                image_out = img_resize = cv2.cvtColor(image_out, cv2.COLOR_BGR2GRAY)


            imgs.append(image_out)
            if show:
                cv2.imshow('image',image_out)
                cv2.waitKey(0)

        return imgs

    def ExecuteAugmentorScriptIndependent(self):
        b = Augmentor.Pipeline(self.folder)
        # b.histogram_equalisation(probability=0.1)
        b.rotate(0.5, 10., 10.)
        b.sample(self.num_of_samples)

        b = Augmentor.Pipeline(self.folder)
        # b.histogram_equalisation(probability=0.1)
        b.shear(1.,12., 12.)
        b.sample(self.num_of_samples)

        b = Augmentor.Pipeline(self.folder)
        # b.histogram_equalisation(probability=0.1)
        b.crop_centre(1., 0.95, randomise_percentage_area=False)
        b.sample(1)

        b = Augmentor.Pipeline(self.folder)
        # b.histogram_equalisation(probability=0.1)
        b.skew(1.0, magnitude=0.4)
        b.sample(self.num_of_samples*2)

        # b = Augmentor.Pipeline(self.folder)
        # b.histogram_equalisation(probability=0.1)
        # b.rotate(0.3, 12., 12.)
        # b.shear(0.3,15., 15.)
        # b.skew(0.3, magnitude=0.5)
        # b.sample(self.num_of_samples)


    def ExecuteAugmentorScriptIntegrated(self):
        b = Augmentor.Pipeline(self.folder)
        b.crop_centre(0.30, 0.95, randomise_percentage_area=False)
        b.rotate(0.30, 10., 10.)
        b.shear(0.30,12., 12.)
        b.skew(0.30, magnitude=0.4)
        b.sample(self.num_of_samples)







if __name__ == "__main__":

    path_to_data = "/home/ralvarez/Documentos/Dataset_placas/validate_chars_detection/"
    path_to_data ="/home/ralvarez/Documentos/Dataset_placas/Cropped_Plates/"
    path_to_data ="/home/ssi_ralvarez/Documentos/Dataset_placas/Cropped_Plates/"

    a = AugmentatorController(path_to_data,15000/4)
    # a.ExecuteAugmentorScriptIntegrated()
    a.ExecuteAugmentorScriptIndependent()
    # shutil.rmtree(path_to_data+'output',ignore_errors=True)

    # c = Augmentor.Pipeline(path_to_data)
    # c.histogram_equalisation(probability=0.1)
    # c.rotate(0.5, 15., 15.)
    # c.sample(num_of_samples/3)
    # p.gaussian_distortion(0.5, 10,10,8, "bell", "in", mex=0.5, mey=0.5, sdx=0.05, sdy=0.05)

